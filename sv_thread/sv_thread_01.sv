
module fork_join_test;
  
  reg [3:0] port_en;
  int unsigned duration = 500;
  int unsigned xoff, xon;
  semaphore sem;
  semaphore sem_port[4];
  realtime start_time;
  
  initial begin
    port_en = 4'hf;
    sem = new(1);
    foreach(sem_port[i])
      sem_port[i] = new(1);
    //$display("s[string] = %0d", s);
    //$finish();
  end
  
  task do_xoff(input int i);
    automatic int port_id;
    automatic int xoff_time, xon_time;
    port_id = i;
    //fork
      
    $display("%0dns : port%0d do xoff start", $realtime, port_id);
    do begin
      xoff_time=$urandom_range(100, 10);
      xon_time=$urandom_range(20,1);
      sem.get();
      $display("@%0dns : port%0d start, xoff=%0d, xon=%0d", $realtime(), port_id, xoff_time, xon_time);
      #1;
      sem.put();
      #xoff_time;
      sem.get();
      #1;
      sem.put();
      #xon_time;
      $display("@%0dns : port%0d end, xoff=%0d, xon=%0d", $realtime, port_id, xoff_time, xon_time);
    end while(duration>($realtime()-start_time));
    //join_none
    //wait fork;
  endtask

  initial begin
    #10;
    start_time = $realtime();
    //$display("all xoff&xon start");
    $display("@%0d : all xoff&xon start", $realtime);
      
    foreach(port_en[i]) begin
      automatic int port_id = i;
      //$display("%0dns : get port%0d key start", $realtime, port_id);
      //sem_port[port_id].get();
      //$display("%0dns : get port%0d key end", $realtime, port_id);
      fork
      	do_xoff(port_id);
      join_none
    end
    wait fork;
      
      //foreach(port_en[i]) begin
      //  automatic int port_id;
      //  port_id = i;
      //  fork    
      //    do_xoff(port_id);
      //  join_none
      //end
      //$finish();
    $display("@%0d : all xoff&xon end", $realtime);
    $finish();
    
    
  end
  
endmodule 