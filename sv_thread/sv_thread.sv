
module sv_thread;
  

  reg [3:0] port_en;
  int unsigned duration = 500;
  int unsigned xoff, xon;
  semaphore sem;
  semaphore sem_port[4];
  realtime start_time;
  
  initial begin
    port_en = 4'hf;
    sem = new(1);
    foreach(sem_port[i])
      sem_port[i] = new(1);
  end

  initial begin
    #10;
    start_time = $realtime();
    $display("all xoff&xon start");
    do begin
        foreach(port_en[i]) begin
          sem_port[i].get();
        end
    	foreach(port_en[i]) begin
      		automatic int port_id=i;
      		automatic int xoff_time, xon_time;
     	    xoff_time=$urandom_range(100, 10);
      		xon_time=$urandom_range(20,1);
      		fork
        		begin

          			sem.get();
                    $display("@%0dns : port%0d start, xoff=%0d, xon=%0d", $realtime(), port_id, xoff_time, xon_time);
          			#1;
          			sem.put();
          			#xoff_time;
          			sem.get();
                    #1;
                    sem.put();
          			#xon_time;
                    sem_port[port_id].put();
                    $display("@%0dns : port%0d end, xoff=%0d, xon=%0d", $realtime, port_id, xoff_time, xon_time);
                    
                    
        		end
                $display("@%0dns : port%0d rand start[get_key_start]", $realtime, port_id);
                sem_port[port_id].get();
                $display("@%0dns : port%0d rand start[get_key_end]", $realtime, port_id);
            join_none
            //sem_port[port_id].get();

        end
    end while(duration>($realtime()-start_time));
    $display("@%0d : all xoff&xon done", $realtime);
    
    
  end
  
endmodule 