// Code your testbench here
// or browse Examples

module tb;
  `include "sv_manul_randc.sv"
  
  sv_manul_randc sv_randc;

  int test_data;
    
  initial begin : Q_INI
    sv_randc = new();
  end
  
  initial begin
    #10ns;
    for(int i=0;i<32;i++) begin
      sv_randc.randomize();
      test_data = sv_randc.test_data;
      $display("the %0d test data = %0d.", i, test_data);
    end
  end
  
  
  
endmodule