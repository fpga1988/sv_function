
class sv_manul_randc;
  
  typedef int randc_t[$];
  randc_t ini_q, randc_queue;
  int test_data;
    
  
  function randc_t gen_random_queue();
    randc_t test_q;
    if(ini_q.size() == 0) begin
      for(int i=0;i<16;i++)
        ini_q.push_back(i);
    end
    test_q = ini_q;
    test_q.shuffle();
    $display("gen random queue : %p.", test_q);
    return test_q;
  endfunction
  
  function int sv_randc(ref randc_t rand_queue);
    int rand_data;
    rand_data = rand_queue.pop_front();
    return rand_data;
  endfunction : sv_randc
  
  
  function void post_randomize();
    if(randc_queue.size() == 0)
      randc_queue = gen_random_queue();
    test_data = sv_randc(randc_queue);
    //$display("randomized test data = %0d.", test_data);
  endfunction
  

  
endclass